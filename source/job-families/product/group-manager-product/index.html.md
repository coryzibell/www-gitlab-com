---
layout: job_family_page
title: "Group Manager, Product"
---

As the Group Manager, Product, you will be responsible for managing and building
a team that focuses on a subset of GitLab's [product categories](/handbook/product/categories/#devops-stages).  This role typically manages 2-4 Product Managers, and reports either to a Director of Product or the VP of Product.

## Individual responsibility

- Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Work on the vision with the Director of Product, VP of Product Strategy, VP of Product, and CEO; and communicate this vision internally and externally
- Distill the overall vision into a compelling roadmap
- Make sure the vision advances in every release and communicate this
- Communicate our vision though demo's, conference speaking, blogging, and interviews
- Work closely with Product Marketing, Sales, Engineering, etc.

## Team responsibility

- Ensure that the next milestone contains the most relevant items to customers, users, and us
- Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
- Make sure the [DevOps tools](/devops-tools/) are up to date
- Keep [/direction](/direction) up to date as our high level roadmap
- Regularly join customer and partner visits that can lead to new features
- Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
- Make sure the release announcements are attractive and cover everything
- Be present on social media (hacker news, twitter, stack overflow, mailinglist), especially around releases

## Requirements

* 1-3 years experience managing others
* 4-6 years of experience in product management
* Experience in DevOps
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* Additional requirements are outlined in the [Product Management Career Development Framework](/handbook/product/#product-management-career-development-framework)
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values), and work in accordance with those values
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

## Primary Performance Indicators for the Role
[Stage Monthly Active Users](https://about.gitlab.com/handbook/product/metrics/#adoption)
[Category Maturity Achievement](https://about.gitlab.com/handbook/product/metrics/)