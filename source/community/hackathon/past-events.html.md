---
layout: markdown_page
title: "Past GitLab Hackathon Events"
---

## Q3'2019 Hackathon (August 28-29)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/27)
* [Event recap blog post](https://about.gitlab.com/blog/2019/10/01/q3-hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| Hackathon kickoff | David Planella/Ray Paik (@dplanella/@rpaik) | [Slides](https://docs.google.com/presentation/d/1rap7hpo89sB-Z-NAI5qZGN0SFVArPoeV5_QzoPl24xQ/)/[Video](https://www.youtube.com/embed/oP_mNw-60zg) |
| Testing at GitLab | Ramya Authappan (@at.ramya) | [Slides](https://docs.google.com/presentation/d/1ZtbwXJwsYxkN9jwDA7MXPkmu6_Y1nu_hnA0EZiNBIDw/)/[Video](https://www.youtube.com/embed/223BfV20JT4) |
| GitLab Design System | George Tsiolis (@gtsiolis)/Taurie Davis (@tauriedavis) | [Slides](https://docs.google.com/presentation/d/1vMLivyv4ZPOTFmUk-c3etzGesRFaaLjdLzGM3SFVXT8/)/[Video](https://www.youtube.com/embed/Zp-jJNQJhZk) |
| GitLab Package Stage | Tim Rizzi (@trizzi) | [Slides](https://docs.google.com/presentation/d/1HoQT9x01grj2W7SVfWYjc01xrSiy75a-hsd4ul1ZKWo/)/[Video](https://www.youtube.com/embed/s2iZx6foI8k) |
| Hackathon wrap-up/recognizing contributors | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/1opfmq8cKuC8-19gU2OUIKgSlNixX3M6Per0nqcgEmKE/)/[Video](https://www.youtube.com/embed/EMt1F2McwPk) | 

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/24)

## Q2'2019 Hackathon (May 29-30)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/20)
* [Event recap blog post](https://about.gitlab.com/2019/06/24/q2-hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| Hackathon kickoff | David Planella/Ray Paik (@dplanella/@rpaik) | [Slides](https://docs.google.com/presentation/d/1qgsVTjA4VPMD0arY8KOk-RT-Mq_ndGQNVZjZXnutZW4)/[Video](https://youtu.be/jG1aIJCP-J0) |
| Contributing to GitLab | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/1KIvg0a0c1olivan1TM9SUJpWfz1Mpwv9Qavb0OPpcNg)/[Video](https://youtu.be/n2oERHm5ytg) |
| GitLab Release Stage | Jason Lenny (@jlenny) | [Video](https://youtu.be/CC1yn03WWls) |
| Monitor | Joshua Lambert (@joshlambert) | [Video](https://youtu.be/mm_8wVjn808) |
| Event wrap-up | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/1N56NQZUPGlx1n1tEP9ew7PY_KBdZT_fTuTa4iabw-FU/)/[Video](https://youtu.be/w-Ue3f8OZAQ) |

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/18)

## Q1'2019 Hackathon (February 12-13)
* [Event announcement blog post](https://about.gitlab.com/2019/01/14/q1-hackathon-announcement/)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/16)
* [Event recap blog post](https://about.gitlab.com/2019/03/11/q1-hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| Hackathon kickoff | David Planella/Ray Paik (@dplanella/@rpaik) | [Slides](https://docs.google.com/presentation/d/1BZYh93oqh0-NeDh9pSmi75lA88pOigF1oH0yZlfcVMc)/[Video](https://youtu.be/vem9GCtXapE) |
| Manage | Jeremy Watson (@jeremy) | [Video](https://youtu.be/BnSeX9dU0zA) |
| Verify | Brendan O'Leary (@brendan) | [Slides](https://docs.google.com/presentation/d/1y36J3r3IhYGSsatidUh64OcFvgDsrn8VxDj6Snrqqtc)/[Video](https://youtu.be/pDXUYxdaEqE) |
| Create | James Ramsay (@jramsay) | [Slides](https://docs.google.com/presentation/d/117AX4khQwDKZIkyx6ovahrtCE1lW7pynXwUnNnNiKdI/edit)/[Video](https://youtu.be/VD4VFsYu_nA) |
| Configure | Daniel Gruesso (@danielgruesso) | [Slides](https://docs.google.com/presentation/d/1sfFD1ZeezseqZA4a-wGprLLGs5tK9UALbBTShvJW52A/)/[Video](https://youtu.be/12iLmFTBTJ8) |
| Event wrap-up | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/1_Wt_2iH96F6FtUbV2OkeT1xskw9M1SShd-8Y83UA2-Y/)/[Video](https://youtu.be/I6-UATiGs3Q) |

*  [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/14)

## Q4'2018 Hackathon (November 14-15)
* [Event announcement blog post](https://about.gitlab.com/2018/10/23/q4-hackathon-announcement/)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/9)
* [Event recap blog post](https://about.gitlab.com/2018/11/29/q4-hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| GitLab community | David Planella/Ray Paik (@dplanella/@rpaik) | [Slides](https://docs.google.com/presentation/d/176wP9znxdz_egFx3wh7w9fSCd1oOGjLAfrnzrt1yRBs/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=BujIJE5N0Rc) |
| Omnibus | DJ Mountney (@twk3) | [Video](https://www.youtube.com/watch?v=mCec7g6Ml70) |
| GitLab Application Architecture | Stan Hu (@stanhu) | [Slides](https://docs.google.com/presentation/d/1qigstMdjhFmulRGo3f-NeFOBN_JXsF_3a8zJblo11zY)/[Video](https://www.youtube.com/watch?v=0GVtrxZ5_a8) |
| Gitter | Eric Eastwood (@MadLittleMods) | [Video](https://www.youtube.com/watch?v=LFWTW6PbJOQ) |
| Event wrap-up | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/11nsZ_4pJDEVSfTUKIFOZYH1mg7QVw0qOO71JmyrWPIU/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=jvDKdrHRoZ0) |

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/10)

## Q3'2018 Hackathon (September 27-28)

* [Event announcement blog post](/2018/09/17/gitlab-hackathon/)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/1)
* [Event recap blog post](/2018/10/09/hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| Event kickoff | David Planella(@dplanella)/Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/1udT8v45w6LoaIGZmqqMN0v0dn1mqcMkZ1PPcfdwiA9k/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=v1QNNpZ79mA) |
| GitLab Development Kit (GDK) | Toon Claes (@toon) | [Video](https://www.youtube.com/watch?v=gxn-0KSfNaU) |
| Documentation | Mike Lewis(@mikelewis)/ Achilleas Pipinellis(@axil) | [Slides](https://docs.google.com/presentation/d/1ZpjBPS1gG0FKPgX7zxfgRjK_1YzApdw1M1CedsRrDfk/)/[Video](https://youtu.be/8GT2XOkpSi4) |
| Internationalization/Translation | Hannes Rosenögger(@haynes) | [Video](https://youtu.be/LJ9oSSx0qyY) |
| Day 1 wrap-up | Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/1jtIs00GOGaweozR_rzJ0XR2QePxi_1GXA3Kl-cvTchk/edit#slide=id.g42dc46e089_0_9)/[Video](https://youtu.be/tONnxJ0_yEM)|
| UX design workflow | Sarrah Vesselov (@sarrahvesselov) | [Slides](https://docs.google.com/presentation/d/1wKjRc7tXeinjwfwFZstjLzIrea1gLc2VJIUagApCnBI)/[Video](https://youtu.be/q_nq5OCiktE) |
| Merge Request Coach | Clement Ho (@ClemMakesApps) | [Video](https://youtu.be/daCFv9tAQXw)|
| Event wrap-up | Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/18niYSpi7aTZ_Biczn3mPxo8_qyjvYVn_rZW4SjHk6OA/edit#slide=id.g3db14447a7_0_0)/[Video](https://youtu.be/rBwKfBVi4Qw) |

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/4)
