---
layout: markdown_page
title: "Ally resources"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is an ally? 

A diversity and inclusion "ally" is someone who is willing to take action in support of another person, in order to remove barriers that impede that person from contributing their skills and talents in the workplace or community.

## How to be an ally 

It is not required to be an ally to work at GitLab. At GitLab it is required to [be inclusive](/handbook/values/#diversity--inclusion). Being an ally goes a step beyond being inclusive to taking action to support marginalized groups. The first step in being an ally is self-educating. This is a page to list resources that GitLab team-members have found helpful in learning how to act as an ally.

## GitLab diversity and inclusion resources 

Allies familize themselves with GitLab's general D&I content

- [Diversity and Inclusion page](/company/culture/inclusion/)
- [D&I training](/company/culture/inclusion/#employee-training-and-learning-opportunities)
- [Unconscious bias](/handbook/communication/unconscious-bias/)

## Resources

Here are additional resources on being an ally

- [Guide to allyship](https://www.guidetoallyship.com)
- [5 Tips For Being An Ally](https://www.youtube.com/watch?v=_dg86g-QlM0)
- [Ally skills workshop](https://frameshiftconsulting.com/ally-skills-workshop/). Check out the materials section with [a handout PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20handout%20-%20Letter.pdf) (linking to many more resources), [slides PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20slides.pdf), [videos](https://www.youtube.com/watch?v=wob68Nl2440), and more.
- [Why cisgender allies should put pronouns on their name tag](https://medium.com/@mrsexsmith/dear-cis-people-who-put-your-pronouns-on-your-hello-my-name-is-nametags-78c047ed7af1)